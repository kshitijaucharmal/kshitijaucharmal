- 👋 Hi, I’m @kshitijaucharmal
- 👀 I’m interested in AI Programming and learning new stuff
- 🌱 I’m currently learning Arduino and microcontrollers integration with games and apps(Python, java, and Unity)
- 💞️ I’m looking to collaborate on ...
- 📫 How to reach me :
  You can find me on my <a href="https://kshitijaucharmal.github.io">website</a>

<!---
kshitijaucharmal/kshitijaucharmal is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
